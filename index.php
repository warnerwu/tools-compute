<?php
// 设置脚本时区
ini_set("date.timezone", "Asia/Shanghai");

// 接收输入计算参数
$params = isset($_POST['params']) ? $_POST['params'] : '';

// 匹配整数或浮点数
preg_match_all('/([0-9]+(\.[0-9]+)?)/', $params, $matches);

// 结果值
$computed_res = null;

// 是否为空
$notNull = false;

// 不为空时输出计算结果
if ( isset( $matches[0] ) && !empty( $matches[0] ) ) {
    $notNull = true;
    $computed_res = array_sum($matches[0]);
} else {
    $computed_res = '请输入计算参数！谢谢...';
}

// 当前时间
$time = date('Y-m-d H:i:s', time());
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="apple-touch-icon" sizes="76x76" href="/resource/images/logo.png">
    <link rel="icon" type="image/png" href="/resource/images/logo.png">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>快速计算</title>
    <style>
        .mg-t-m-1 {
            margin: 1rem auto;
        }
        .text-align-center {
            text-align: center;
        }
    </style>
    <script>
        function clearParams() {
            var sParams = document.querySelector('input[name="params"]');
            sParams.setAttribute('value', '');
            sParams.value = '';
        }
    </script>
</head>
<body>
    <!-- Image and text -->
    <nav class="navbar navbar-light bg-light">
        <div class="container">
            <a class="navbar-brand" href="/">
                <img src="./resource/images/logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
                快速计算
            </a>
        </div>
    </nav>
    <div class="container">
        <form class="mg-t-m-1" method="post">
            <div class="form-group">
                <input type="text" name="params" class="form-control" placeholder="计算参数" value="<?php if( !empty( $params ) ) echo $params; ?>">
            </div>
            <button type="submit" class="btn btn-primary btn-block">计算</button>
            <button type="button" class="btn btn-outline-success btn-block" onclick="clearParams()">清除</button>
        </form>

        <?php
            if ( $computed_res !== null ) {
                ?>
                <div class="alert alert-success text-align-center" role="alert">
                    <?php
                    if( $notNull === true ) {
                        echo '计算时间: ' . $time . ', 计算结果: ' . $computed_res;
                    } else {
                        echo $computed_res;
                    }
                    ?>
                </div>
        <?php
            }
        ?>
    </div>
</body>
</html>
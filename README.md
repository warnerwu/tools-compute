## tools-compute 项目说明

tools-compute 项目用于快速计算多个值的求和操作

### 使用 `composer` 初始化项目目录

> 使用命令

```bash
composer init
```

> 生成 `composer.json` 文件

```json
{
    "name": "warnerwu/compute",
    "description": "快速计算小工具",
    "type": "project",
    "license": "MIT",
    "authors": [
        {
            "name": "warnerwu",
            "email": "warnerwu@126.com"
        }
    ],
    "require" : {
        "silex/silex": "^2.0.4",
        "monolog/monolog": "^1.22",
        "twig/twig": "^2.0",
        "symfony/twig-bridge": "^3"
    },
    "require-dev": {
        "heroku/heroku-buildpack-php": "*"
    },
    "repositories": {
        "packagist": {
            "type": "composer",
            "url": "https://packagist.phpcomposer.com"
        }
    }
}
```


### Heroku 项目部署

#### Heroku 项目初始化

> Heroku 初始化本地项目

```bash
heroku create
```

> 生成如下信息

```bash
➜  contrast heroku create
 ›   Warning: heroku update available from 7.0.41 to 7.0.88
Creating app... done, ⬢ lit-springs-12351
https://fast-ocean-40606.herokuapp.com/ | https://git.heroku.com/fast-ocean-40606.git
```

#### Heroku 项目部署

```bash
git push -u origin heroku
```

#### Heroku 项目预览

```bash
heroku open
```

#### [项目截图](https://fast-ocean-40606.herokuapp.com/)

![项目截图](http://oluvq2vg4.bkt.clouddn.com/image/jpg/tools-compute/Snip20180611_4.png)